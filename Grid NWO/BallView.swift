//
//  BallView.swift
//  Grid NWO
//
//  Created by Sanchit Goel on 08/01/18.
//  Copyright © 2018 Sanchit Goel. All rights reserved.
//

import UIKit

class BallView: UIView {

  var primaryColor: UIColor?
  var secondaryColor: UIColor?
  let primaryShapeLayer = CAShapeLayer()
  let secondaryShapeLayer = CAShapeLayer()
  var leadingBlocks: CGFloat = 0
  var verticalBlocks: CGFloat = 0
  var primaryIndexpath: IndexPath?
  var secondaryIndexpath: IndexPath?
  
  init(frame: CGRect, primaryColor: UIColor, secondaryColor: UIColor, primaryIndex: IndexPath, secondaryIndex: IndexPath) {
    super.init(frame: frame)
    self.primaryColor = primaryColor
    self.secondaryColor = secondaryColor
    self.primaryIndexpath = primaryIndex
    self.secondaryIndexpath = secondaryIndex
    initialSetup(leftCurvePath: createLeftCurve(), rightCurvePath: createRightCurve())
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func initialSetup(leftCurvePath: UIBezierPath, rightCurvePath: UIBezierPath) {
    primaryShapeLayer.path = leftCurvePath.cgPath
    primaryShapeLayer.strokeColor = UIColor.black.cgColor
    primaryShapeLayer.fillColor = primaryColor?.cgColor
    primaryShapeLayer.lineWidth = 1.0
    self.layer.addSublayer(primaryShapeLayer)
    secondaryShapeLayer.path = rightCurvePath.cgPath
    secondaryShapeLayer.strokeColor = UIColor.black.cgColor
    secondaryShapeLayer.fillColor = secondaryColor?.cgColor
    secondaryShapeLayer.lineWidth = 1.0
    self.layer.addSublayer(secondaryShapeLayer)
  }
  
  func createLeftCurve() -> UIBezierPath {
    let leftCurvePath = UIBezierPath()
    let start = CGPoint(x: self.frame.width/2.0, y:self.frame.height/2.0)
    leftCurvePath.move(to: start)
    leftCurvePath.addLine(to: CGPoint(x: self.frame.width/2.0, y: 0))
    leftCurvePath.addArc(withCenter: start, radius: self.frame.size.height/2, startAngle: 3*CGFloat(Double.pi)/2, endAngle: CGFloat(Double.pi)/2 , clockwise: false)
    leftCurvePath.close()
    return leftCurvePath
  }
  
  func createRightCurve() -> UIBezierPath {
    let rightCurvePath = UIBezierPath()
    let start = CGPoint(x: self.frame.width/2.0, y: self.frame.height/2.0)
    rightCurvePath.move(to: start)
    rightCurvePath.addLine(to: CGPoint(x: self.frame.width/2.0, y: 0))
    rightCurvePath.addArc(withCenter: start, radius: self.frame.size.height/2, startAngle: 3*CGFloat(Double.pi)/2, endAngle: CGFloat(Double.pi)/2 , clockwise: true)
    rightCurvePath.close()
    return rightCurvePath
  }
}
