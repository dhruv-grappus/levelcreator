//
//  ViewController.swift
//  Grid NWO
//
//  Created by Sanchit Goel on 09/10/17.
//  Copyright © 2017 Sanchit Goel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  // MARK: IBOutlets
  @IBOutlet weak var editBtn: UIButton!
  @IBOutlet weak var pickerViewBottomConstraint: NSLayoutConstraint!
  @IBOutlet weak var createView: UIView!
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var gridView: UIView!
  @IBOutlet weak var pickerView: UIPickerView!
  @IBOutlet weak var verticalBtn: UIButton!
  @IBOutlet weak var horizontalBtn: UIButton!
  @IBOutlet weak var deleteViewBtn: UIButton!
  @IBOutlet weak var modifyViewBtn: UIButton!
  @IBOutlet weak var addViewBtn: UIButton!
  @IBOutlet weak var addBallBtn: UIButton!
  @IBOutlet weak var addExitBtn: UIButton!
  @IBOutlet weak var primaryColorCollectionView: UICollectionView!
  @IBOutlet weak var secondaryColorCollectionView: UICollectionView!
  @IBOutlet weak var pathColorCollectionView: UICollectionView!
  @IBOutlet weak var blurView: UIView!
  @IBOutlet weak var singleColorView: UIView!
  @IBOutlet weak var multipleColorView: UIView!
  @IBOutlet weak var deleteBallBtn: UIButton!
  @IBOutlet weak var modifyBallBtn: UIButton!
  @IBOutlet weak var addBallViewBtn: UIButton!
  
  // MARK: Variables
  let screenHeight:CGFloat = UIScreen.main.bounds.height
  let screenWidth:CGFloat = UIScreen.main.bounds.width
  var previousLocation: CGPoint = CGPoint(x: 0, y: 0)
  let kThickness:CGFloat = 12.0
  let verticalBlocksInGrid:CGFloat = 80
  let horizontalBlocksInGrid:CGFloat = 46
  
  var pathsArray = [PathView]()
  var viewOrientation: String?
  var numberOfBlocks:Int?
  var viewColor: UIColor?
  
  var ballsArray = [BallView]()
  var primaryColor: UIColor?
  var secondaryColor: UIColor?
  
  var exitViewObject: UIView?
  
  var pickerData = [Int]()
  
  var pathPresent: Bool = false
  var ballPresent: Bool = false
  var exitPresent: Bool = false
  
  var selectedPath:Int = 0
  var selectedBall:Int = 0
  var editMode:Bool = false
  
  var pathDetails: [String : Any] = ["xPosition" : [CGFloat](),
                                     "yPosition": [CGFloat](),
                                     "color": [String](),
                                     "orientation": [String](),
                                     "numberOfBlocks": [Int](),
                                     "exitPathXPosition": Int(),
                                     "exitPathYPosition": Int()
  ]
  
  var ballDetails: [String : Any] = ["xPosition": [CGFloat](),
                                     "yPosition": [CGFloat](),
                                     "primaryColor": [String](),
                                     "secondaryColor": [String](),
                                     ]
  var exitPathPosition: CGPoint?
  var levelDetails: [String: [String:Any]] = ["Path": [String : Any](),
                                              "Ball": [String : Any](),
                                              ]
  var colorArray:[UIColor] = [UIColor.red, UIColor.blue, UIColor.yellow, UIColor.orange, UIColor.green]
  var primarySelectedIndexpath = IndexPath(item: 0, section: 0)
  var secondarySelectedIndexpath = IndexPath(item: 0, section: 0)
  var selectedPathColorIndexpath = IndexPath(item: 0, section: 0)
  
  // Path Deatils
  var pathXPosition = [CGFloat]()
  var pathYPosition = [CGFloat]()
  var pathColor = [UIColor]()
  var pathColorHexCode = [String]()
  var pathNumberOfBlocks = [Int]()
  var pathOrientation = [String]()
  
  // Ball Details
  var ballPositionPoints = [CGPoint]()
  var ballXPostion = [CGFloat]()
  var ballYPosition = [CGFloat]()
  var ballPrimaryColor = [UIColor]()
  var ballPrimaryColorHexCode = [String]()
  var ballSecondaryColor = [UIColor]()
  var ballSecondaryColorhexCode = [String]()
  
  // MARK: Lifecycle methods
  override func viewDidLoad() {
    super.viewDidLoad()
    createView.alpha = 0.0
    addPickerData()
    addLines()
    setupUI()
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ViewController.exitCreateViewTapped(recognizer:)))
    self.blurView.addGestureRecognizer(tapGesture)
  }
  
  // MARK: Default methods
  func addPickerData() {
    for i in 1...50 {
      pickerData.append(i)
    }
  }
  
  func setupUI() {
    horizontalBtn.layer.borderWidth = 1.0
    verticalBtn.layer.borderWidth = 1.0
    horizontalBtn.layer.borderColor = UIColor.white.cgColor
    verticalBtn.layer.borderColor = UIColor.white.cgColor
    multipleColorView.isHidden = true
    singleColorView.isHidden = true
  }
  
  func addLines() {
    for i in 0...Int(verticalBlocksInGrid) {
      let path = createPath(initialPoint:CGPoint(x:screenWidth/2 - horizontalBlocksInGrid*kThickness/2.0, y:CGFloat(CGFloat(i)*kThickness) + screenHeight/2 - verticalBlocksInGrid*kThickness/2 ) , finalPoint:CGPoint(x:screenWidth/2 + horizontalBlocksInGrid*kThickness/2.0, y:CGFloat(CGFloat(i)*kThickness) + screenHeight/2 - verticalBlocksInGrid*kThickness/2.0) , horizontal: true)
      if CGFloat(i + 2).truncatingRemainder(dividingBy: 6) == 0 {
        path.backgroundColor = UIColor.black
      } else {
        path.backgroundColor = UIColor.lightGray
      }
      if i == Int(verticalBlocksInGrid)/2 {
        path.backgroundColor = UIColor.red
      }
      gridView.addSubview(path)
    }
    for i in 0...Int(horizontalBlocksInGrid) {
      let path1 = createPath(initialPoint:CGPoint(x:CGFloat(i)*kThickness + screenWidth/2 - horizontalBlocksInGrid*kThickness/2, y:screenHeight/2 - verticalBlocksInGrid*kThickness/2 ) , finalPoint:CGPoint(x:CGFloat(i)*kThickness + screenWidth/2 - horizontalBlocksInGrid*kThickness/2, y:screenHeight/2 + verticalBlocksInGrid*kThickness/2) , horizontal: false)
      if CGFloat(i + 1).truncatingRemainder(dividingBy: 6) == 0 {
        path1.backgroundColor = UIColor.black
      } else {
        path1.backgroundColor = UIColor.lightGray
      }
      if i == Int(horizontalBlocksInGrid)/2 {
        path1.backgroundColor = UIColor.red
      }
      gridView.addSubview(path1)
    }
  }
  
  func createPath(initialPoint:CGPoint, finalPoint:CGPoint, horizontal: Bool) -> GridView {
    var rect = CGRect.zero
    if horizontal {
      rect = CGRect(x: initialPoint.x, y: initialPoint.y, width: finalPoint.x - initialPoint.x , height: 1)
    } else {
      rect = CGRect(x: initialPoint.x, y: initialPoint.y, width: 1 , height: finalPoint.y - initialPoint.y)
    }
    let grid = GridView(frame: rect , initialPoint: initialPoint, finalPoint: finalPoint, horizontal: horizontal)
    return grid
  }
  
  // MARK: IBActions
  @IBAction func editPaths(_ sender: Any) {
    if editBtn.titleLabel?.text == "Edit" {
      editMode = true
      editBtn.setTitle("Done", for: .normal)
      modifyViewBtn.isHidden = false
      deleteViewBtn.isHidden = false
      addViewBtn.isHidden = true
    } else {
      editMode = false
      editBtn.setTitle("Edit", for: .normal)
      modifyViewBtn.isHidden = true
      deleteViewBtn.isHidden = true
      addViewBtn.isHidden = false
    }
  }
  
  @IBAction func horizontalSelected(_ sender: Any) {
    UIView.animate(withDuration: 0.1,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.horizontalBtn.layer.borderColor = UIColor.black.cgColor
                    self.verticalBtn.layer.borderColor = UIColor.white.cgColor
    }, completion: nil
    )
    viewOrientation = "Horizontal"
  }
  
  @IBAction func verticalSelected(_ sender: Any) {
    UIView.animate(withDuration: 0.1,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.horizontalBtn.layer.borderColor = UIColor.white.cgColor
                    self.verticalBtn.layer.borderColor = UIColor.black.cgColor
    }, completion: nil
    )
    viewOrientation = "Vertical"
  }
  
  @IBAction func doneBtnTapped(_ sender: Any) {
    numberOfBlocks = pickerView.selectedRow(inComponent: 0) + 1
    textField.text = "\(pickerView.selectedRow(inComponent: 0) + 1)"
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.pickerViewBottomConstraint.constant = -200
    }, completion: nil
    )
  }
  
  @IBAction func addPathViewTapped(_ sender: Any) {
    viewColor = colorArray[selectedPathColorIndexpath.item]
    if let color = viewColor, let orientation = viewOrientation, let number = numberOfBlocks {
      UIView.animate(withDuration: 0.4,
                     delay: 0,
                     options: .curveEaseIn,
                     animations: {
                      self.createView.alpha = 0.0
      }, completion: nil
      )
      if orientation == "Horizontal" {
        let rect = CGRect(x: screenWidth/2 - CGFloat(Int(number)/2) * kThickness, y: screenHeight/2 - kThickness, width: kThickness * CGFloat(number) + 1, height: kThickness + 1)
        let myView = PathView(frame: rect, color: color, orientation: orientation, number: number, indexpath: selectedPathColorIndexpath)
        myView.layer.borderWidth = 1.0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pathViewTapped(gesture:)))
        myView.addGestureRecognizer(panGesture)
        pathsArray.append(myView)
        gridView.addSubview(myView)
      } else {
        let rect = CGRect(x: screenWidth/2 - kThickness, y: screenHeight/2 - CGFloat(Int(number)/2) * kThickness, width: kThickness + 1, height: kThickness * CGFloat(number) + 1)
        let myView = PathView(frame: rect, color: color, orientation: orientation, number: number, indexpath: selectedPathColorIndexpath)
        myView.layer.borderWidth = 1.0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pathViewTapped(gesture:)))
        myView.addGestureRecognizer(panGesture)
        pathsArray.append(myView)
        gridView.addSubview(myView)
      }
      viewColor = nil
      viewOrientation = nil
      textField.text = nil
    } else {
      alertify(message: "Incorrect Input", in: self)
    }
  }
  
  @IBAction func deleteViewTapped(_ sender: Any) {
    pathsArray[selectedPath].removeFromSuperview()
    pathsArray.remove(at: selectedPath)
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 0.0
    }, completion: nil
    )
  }
  
  @IBAction func modifyViewTapped(_ sender: Any) {
    editBtn.setTitle("Edit", for: .normal)
    editMode = false
    addViewBtn.isHidden = false
    deleteViewBtn.isHidden = true
    modifyViewBtn.isHidden = true
    pathsArray[selectedPath].removeFromSuperview()
    pathsArray.remove(at: selectedPath)
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 0.0
    }, completion: nil
    )
    viewColor = colorArray[selectedPathColorIndexpath.item]
    if let color = viewColor, let orientation = viewOrientation, let number = numberOfBlocks {
      if orientation == "Horizontal" {
        let rect = CGRect(x: screenWidth/2 - CGFloat(Int(number)/2) * kThickness, y: screenHeight/2 - kThickness, width: kThickness * CGFloat(number) + 1, height: kThickness + 1)
        let myView = PathView(frame: rect, color: color, orientation: orientation, number: number, indexpath: selectedPathColorIndexpath)
        myView.layer.borderWidth = 1.0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pathViewTapped(gesture:)))
        myView.addGestureRecognizer(panGesture)
        pathsArray.insert(myView, at: selectedPath)
        gridView.addSubview(myView)
      } else {
        let rect = CGRect(x: screenWidth/2 - kThickness, y: screenHeight/2 - CGFloat(Int(number)/2) * kThickness, width: kThickness + 1, height: kThickness * CGFloat(number) + 1)
        let myView = PathView(frame: rect, color: color, orientation: orientation, number: number, indexpath: selectedPathColorIndexpath)
        myView.layer.borderWidth = 1.0
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(pathViewTapped(gesture:)))
        myView.addGestureRecognizer(panGesture)
        pathsArray.insert(myView, at: selectedPath)
        gridView.addSubview(myView)
      }
    }
  }
  
  @IBAction func enterBlocksTapped(_ sender: Any) {
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.pickerViewBottomConstraint.constant = 0
    }, completion: nil
    )
  }
  
  @IBAction func addBallViewTapped(_ sender: Any) {
    primaryColor = colorArray[primarySelectedIndexpath.item]
    secondaryColor = colorArray[secondarySelectedIndexpath.item]
    if let primary = primaryColor, let secondary = secondaryColor {
      UIView.animate(withDuration: 0.4,
                     delay: 0,
                     options: .curveEaseIn,
                     animations: {
                      self.createView.alpha = 0.0
      }, completion: nil
      )
      let rect = CGRect(x: screenWidth/2.0 - kThickness * 1.5, y: screenHeight/2.0 - kThickness * 1.5, width: kThickness * 3.0, height: kThickness * 3.0)
      let ball = BallView(frame: rect, primaryColor: primary, secondaryColor: secondary, primaryIndex: primarySelectedIndexpath, secondaryIndex: secondarySelectedIndexpath)
      let panGesture = UIPanGestureRecognizer(target: self, action: #selector(ballViewTapped(gesture:)))
      ball.addGestureRecognizer(panGesture)
      ball.isUserInteractionEnabled = true
      ballsArray.append(ball)
      gridView.addSubview(ball)
    }
  }
  
  @IBAction func deleteBallViewTapped(_ sender: Any) {
    ballsArray[selectedBall].removeFromSuperview()
    ballsArray.remove(at: selectedBall)
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 0.0
    }, completion: nil
    )
  }
  
  @IBAction func modifyBallViewTapped(_ sender: Any) {
    editBtn.setTitle("Edit", for: .normal)
    editMode = false
    addBallBtn.isHidden = false
    deleteBallBtn.isHidden = true
    modifyBallBtn.isHidden = true
    ballsArray[selectedBall].removeFromSuperview()
    ballsArray.remove(at: selectedBall)
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 0.0
    }, completion: nil
    )
    primaryColor = colorArray[primarySelectedIndexpath.item]
    secondaryColor = colorArray[secondarySelectedIndexpath.item]
    if let primary = primaryColor, let secondary = secondaryColor {
      let rect = CGRect(x: screenWidth/2 - kThickness * 1.5, y: screenHeight/2 - kThickness * 1.5, width: kThickness * 3.0, height: kThickness * 3.0)
      let ball = BallView(frame: rect, primaryColor: primary, secondaryColor: secondary, primaryIndex: primarySelectedIndexpath, secondaryIndex: secondarySelectedIndexpath)
      let panGesture = UIPanGestureRecognizer(target: self, action: #selector(ballViewTapped(gesture:)))
      ball.addGestureRecognizer(panGesture)
      ball.isUserInteractionEnabled = true
      ballsArray.insert(ball, at: selectedBall)
      gridView.addSubview(ball)
    }
  }
  
  @IBAction func addBallTapped(_ sender: Any) {
    singleColorView.isHidden = true
    multipleColorView.isHidden = false
    showCreateBallMenu()
  }
  
  @IBAction func addExitTapped(_ sender: Any) {
    let rect = CGRect(x: screenWidth/2 - kThickness/2.0, y: screenHeight/2 - kThickness/2.0, width: kThickness, height: kThickness)
    let exitView = UIView(frame: rect)
    exitView.backgroundColor = UIColor.black
    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(exitViewTapped(gesture:)))
    exitView.addGestureRecognizer(panGesture)
    gridView.addSubview(exitView)
    exitViewObject = exitView
  }
  
  @IBAction func submitLevelTapped(_ sender: Any) {
    if pathsArray.count == 0 {
      alertify(message: "Paths Not Added", in: self)
    } else {
      pathXPosition = [CGFloat]()
      pathYPosition = [CGFloat]()
      pathColor = [UIColor]()
      pathNumberOfBlocks = [Int]()
      pathOrientation = [String]()
      storePathDetails()
    }
    if exitViewObject == nil {
      alertify(message: "Exit Path Not Added", in: self)
    } else {
      storeExitPathDetails()
    }
    if ballsArray.count < 1 {
      alertify(message: "Balls Not Added", in: self)
    } else {
      ballXPostion = [CGFloat]()
      ballYPosition = [CGFloat]()
      ballPrimaryColor = [UIColor]()
      ballSecondaryColor = [UIColor]()
      storeBallDetails()
      alertify(message: "Level Submitted Successfully", in: self)
    }
    
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: levelDetails, options: .prettyPrinted)
      
      let fileManager = FileManager.default
      let filePath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("level1 .json")
      fileManager.createFile(atPath: filePath, contents: jsonData, attributes: nil)
      
      // for reading data stored
      var readStringProject = ""
      do {
        readStringProject = try String(contentsOfFile: filePath, encoding: String.Encoding.utf8)
      } catch let error as NSError {
        print("Failed reading from URL: , Error: " + error.localizedDescription)
      }
    } catch {
      print(error.localizedDescription)
    }
  }
  
  // MARK: Custom Methods
  
  // store details of objects
  func storePathDetails() {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2)*kThickness
    for i in 0...pathsArray.count - 1 {
      let leadingInt = floorf(Float((endX - pathsArray[i].frame.origin.x)/kThickness))
      let verticalInt = floorf(Float((endY - pathsArray[i].frame.origin.y)/kThickness))
      pathsArray[i].leadingBlocks = horizontalBlocksInGrid - CGFloat(leadingInt)
      pathsArray[i].verticalBlocks = verticalBlocksInGrid - CGFloat(verticalInt)
      pathXPosition.append(pathsArray[i].leadingBlocks)
      pathYPosition.append(pathsArray[i].verticalBlocks)
      pathColor.append(pathsArray[i].color!)
      pathOrientation.append(pathsArray[i].orientation!)
      pathNumberOfBlocks.append(pathsArray[i].numberOfBlocks!)
    }
    for color in pathColor {
      if let hexCode = color.toHex(alpha: false) {
        pathColorHexCode.append(hexCode)
      }
    }
    pathDetails["xPosition"] = pathXPosition
    pathDetails["yPosition"] = pathYPosition
    pathDetails["color"] = pathColorHexCode
    pathDetails["orientation"] = pathOrientation
    pathDetails["numberOfBlocks"] = pathNumberOfBlocks
  }
  
  func storeBallDetails() {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2.0)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2.0)*kThickness
    for i in 0...ballsArray.count - 1 {
      ballsArray[i].leadingBlocks = horizontalBlocksInGrid - CGFloat(floorf(Float((endX - ballsArray[i].frame.origin.x)/kThickness)))
      ballsArray[i].verticalBlocks = verticalBlocksInGrid - CGFloat(floorf(Float((endY - ballsArray[i].frame.origin.y)/kThickness)))
      ballXPostion.append(ballsArray[i].leadingBlocks)
      ballYPosition.append(ballsArray[i].verticalBlocks)
      ballPrimaryColor.append(ballsArray[i].primaryColor!)
      ballSecondaryColor.append(ballsArray[i].secondaryColor!)
    }
    for color in ballPrimaryColor {
      if let hexCode = color.toHex(alpha: false) {
        ballPrimaryColorHexCode.append(hexCode)
      }
    }
    for color in ballSecondaryColor {
      if let hexCode = color.toHex(alpha: false) {
        ballSecondaryColorhexCode.append(hexCode)
      }
    }
    
    ballDetails["xPosition"] = ballXPostion
    ballDetails["yPosition"] = ballYPosition
    ballDetails["primaryColor"] = ballPrimaryColorHexCode
    ballDetails["secondaryColor"] = ballSecondaryColorhexCode
    pathDetails["exitPathXPosition"] = exitPathPosition?.x
    pathDetails["exitPathYPosition"] = exitPathPosition?.y
    levelDetails["Path"] = pathDetails
    levelDetails["Ball"] = ballDetails
  }
  
  func storeExitPathDetails() {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2.0)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2.0)*kThickness
    if let exit = exitViewObject {
      let xpos = horizontalBlocksInGrid - CGFloat(floorf(Float((endX - exit.frame.origin.x)/kThickness))) - 1
      let ypos = verticalBlocksInGrid - CGFloat(floorf(Float((endY - exit.frame.origin.y)/kThickness))) - 1
      exitPathPosition = CGPoint(x: xpos, y: ypos)
    }
  }
  
  // gesture handling of objects
  @objc func pathViewTapped(gesture:UIPanGestureRecognizer) {
    let location = gesture.location(in: view)
    var minX: CGFloat = 0.0
    var maxX: CGFloat = 0.0
    var minY: CGFloat = 0.0
    var maxY: CGFloat = 0.0
    if let viewMoved = gesture.view as? PathView{
      
      if viewMoved.orientation == "Horizontal" {
        minX = screenWidth/2.0 - ((horizontalBlocksInGrid/2.0) * kThickness) + CGFloat(viewMoved.numberOfBlocks!)/2.0 * kThickness
        maxX = screenWidth/2.0 + ((horizontalBlocksInGrid/2.0) * kThickness) - CGFloat(viewMoved.numberOfBlocks!)/2.0 * kThickness
        minY = screenHeight/2.0 - ((verticalBlocksInGrid/2.0) * kThickness) + kThickness/2.0
        maxY = screenHeight/2.0 + ((verticalBlocksInGrid/2.0) * kThickness) - kThickness/2.0
      } else {
        minX = screenWidth/2.0 - ((horizontalBlocksInGrid/2.0) * kThickness) + kThickness/2.0
        maxX = screenWidth/2.0 + ((horizontalBlocksInGrid/2.0) * kThickness) - kThickness/2.0
        minY = screenHeight/2.0 - ((verticalBlocksInGrid/2.0) * kThickness)  + CGFloat(viewMoved.numberOfBlocks!)/2.0 * kThickness
        maxY = screenHeight/2.0 + ((verticalBlocksInGrid/2.0) * kThickness)  - CGFloat(viewMoved.numberOfBlocks!)/2.0 * kThickness
      }
      
      if gesture.state == .began {
        previousLocation = location
      }
      
      if gesture.state == .changed {
        if location.x < minX {
          viewMoved.center.x = minX
        }
        if location.x > maxX {
          viewMoved.center.x = maxX + 0.5
        }
        if location.y < minY {
          viewMoved.center.y = minY
        }
        if location.y > maxY {
          viewMoved.center.y = maxY + 0.5
        }
        
        if location.x > minX && location.x < maxX {
          viewMoved.center.x = location.x
        }
        if location.y > minY && location.y < maxY {
          viewMoved.center.y = location.y
        }
      }
      
      if gesture.state == . ended {
        var loc = location
        if location.x < minX {
          loc.x = minX
        }
        if location.x > maxX {
          loc.x = maxX
        }
        if location.y < minY {
          loc.y = minY
        }
        if location.y > maxY {
          loc.y = maxY
        }
        snapViewToGrid(snapView: viewMoved, location: loc)
      }
      previousLocation = location
    }
  }
  
  @objc func ballViewTapped(gesture:UIPanGestureRecognizer) {
    let location = gesture.location(in: view)
    var minX: CGFloat = 0.0
    var maxX: CGFloat = 0.0
    var minY: CGFloat = 0.0
    var maxY: CGFloat = 0.0
    if let ballMoved = gesture.view as? BallView{
      
      minX = screenWidth/2.0 - ((horizontalBlocksInGrid/2.0) * kThickness) + kThickness * 1.5
      maxX = screenWidth/2.0 + ((horizontalBlocksInGrid/2.0) * kThickness) - kThickness * 1.5
      minY = screenHeight/2.0 - ((verticalBlocksInGrid/2.0) * kThickness) + kThickness * 1.5
      maxY = screenHeight/2.0 + ((verticalBlocksInGrid/2.0) * kThickness) - kThickness * 1.5
      
      if gesture.state == .began {
        previousLocation = location
      }
      
      if gesture.state == .changed {
        if location.x < minX {
          ballMoved.center.x = minX
        }
        if location.x > maxX {
          ballMoved.center.x = maxX + 0.5
        }
        if location.y < minY {
          ballMoved.center.y = minY
        }
        if location.y > maxY {
          ballMoved.center.y = maxY + 0.5
        }
        
        if location.x > minX && location.x < maxX {
          ballMoved.center.x = location.x
        }
        if location.y > minY && location.y < maxY {
          ballMoved.center.y = location.y
        }
      }
      
      if gesture.state == . ended {
        var loc = location
        if location.x < minX {
          loc.x = minX
        }
        if location.x > maxX {
          loc.x = maxX
        }
        if location.y < minY {
          loc.y = minY
        }
        if location.y > maxY {
          loc.y = maxY
        }
        snapBallToGrid(snapBall: ballMoved, location: loc)
      }
      previousLocation = location
    }
  }
  
  @objc func exitViewTapped(gesture:UIPanGestureRecognizer) {
    let location = gesture.location(in: view)
    var minX: CGFloat = 0.0
    var maxX: CGFloat = 0.0
    var minY: CGFloat = 0.0
    var maxY: CGFloat = 0.0
    if let exitMoved = gesture.view {
      
      minX = screenWidth/2.0 - ((horizontalBlocksInGrid/2.0) * kThickness) + kThickness/2.0
      maxX = screenWidth/2.0 + ((horizontalBlocksInGrid/2.0) * kThickness) - kThickness/2.0
      minY = screenHeight/2.0 - ((verticalBlocksInGrid/2.0) * kThickness) + kThickness/2.0
      maxY = screenHeight/2.0 + ((verticalBlocksInGrid/2.0) * kThickness) - kThickness/2.0
      
      if gesture.state == .began {
        previousLocation = location
      }
      
      if gesture.state == .changed {
        if location.x < minX {
          exitMoved.center.x = minX
        }
        if location.x > maxX {
          exitMoved.center.x = maxX + 0.5
        }
        if location.y < minY {
          exitMoved.center.y = minY
        }
        if location.y > maxY {
          exitMoved.center.y = maxY + 0.5
        }
        
        if location.x > minX && location.x < maxX {
          exitMoved.center.x = location.x
        }
        if location.y > minY && location.y < maxY {
          exitMoved.center.y = location.y
        }
      }
      
      if gesture.state == . ended {
        var loc = location
        if location.x < minX {
          loc.x = minX
        }
        if location.x > maxX {
          loc.x = maxX
        }
        if location.y < minY {
          loc.y = minY
        }
        if location.y > maxY {
          loc.y = maxY
        }
        snapExitToGrid(snapExit: exitMoved, location: loc)
      }
      previousLocation = location
    }
  }

  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    pathPresent = false
    ballPresent = false
    exitPresent = false
    
    if let touch = touches.first {
      let position = touch.location(in: view)
      if pathsArray.count > 0 {
        for i in 0...pathsArray.count-1 {
          if pathsArray[i].frame.contains(position) {
            pathPresent = true
            selectedPath = i
          }
        }
      }
      if ballsArray.count > 0 {
        for i in 0...ballsArray.count-1 {
          if ballsArray[i].frame.contains(position) {
            ballPresent = true
            selectedBall = i
          }
        }
      }
      if let exit = exitViewObject {
        if exit.frame.contains(position) {
          exitPresent = true
        }
      }
      if editMode {
        if pathPresent {
          showExistingPathDetails(pathIndex: selectedPath)
        }
        if ballPresent {
          showExistingBallDetails(ballIndex: selectedBall)
        }
      } else {
        if !pathPresent && !ballPresent && !exitPresent {
          showCreateViewMenu()
        }
      }
    }
  }
  
  @objc func exitCreateViewTapped(recognizer: UITapGestureRecognizer) {
    print("Tapping working")
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 0.0
    }, completion: nil
    )
  }
  
  // display create objects view
  func showCreateViewMenu() {
    setupCreatePathView()
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 1.0
    }, completion: { _ in
      self.selectedPathColorIndexpath = IndexPath(item: 0, section: 0)
      self.pathColorCollectionView.reloadData()
    })
  }
  
  func showCreateBallMenu() {
    setupCreateBallView()
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 1.0
    }, completion: { _ in
      self.primarySelectedIndexpath = IndexPath(item: 0, section: 0)
      self.primaryColorCollectionView.reloadData()
      self.secondarySelectedIndexpath = IndexPath(item: 0, section: 0)
      self.secondaryColorCollectionView.reloadData()
    })
  }
  
  // display already existing values
  func showExistingPathDetails(pathIndex: Int) {
    deleteViewBtn.isHidden = false
    modifyViewBtn.isHidden = false
    addViewBtn.isHidden = true
    singleColorView.isHidden = false
    multipleColorView.isHidden = true
    
    textField.text = "\(pathsArray[pathIndex].numberOfBlocks!)"
    numberOfBlocks = pathsArray[pathIndex].numberOfBlocks
    viewColor = pathsArray[pathIndex].color
    viewOrientation = pathsArray[pathIndex].orientation
    
    if viewOrientation == "Horizontal" {
      horizontalBtn.layer.borderColor = UIColor.black.cgColor
      verticalBtn.layer.borderColor = UIColor.white.cgColor
    } else {
      horizontalBtn.layer.borderColor = UIColor.white.cgColor
      verticalBtn.layer.borderColor = UIColor.black.cgColor
    }
    if let index = pathsArray[pathIndex].selectedColorIndex {
      selectedPathColorIndexpath = index
      pathColorCollectionView.reloadData()
    }
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 1.0
    }, completion: nil
    )
  }
  
  func showExistingBallDetails(ballIndex: Int) {
    deleteBallBtn.isHidden = false
    modifyBallBtn.isHidden = false
    addBallViewBtn.isHidden = true
    singleColorView.isHidden = true
    multipleColorView.isHidden = false
    
    primaryColor = colorArray[primarySelectedIndexpath.item]
    secondaryColor = colorArray[secondarySelectedIndexpath.item]
    
    if let primaryindex = ballsArray[ballIndex].primaryIndexpath, let secondaryIndex = ballsArray[ballIndex].secondaryIndexpath {
      primarySelectedIndexpath = primaryindex
      secondarySelectedIndexpath = secondaryIndex
      primaryColorCollectionView.reloadData()
      secondaryColorCollectionView.reloadData()
    }
    UIView.animate(withDuration: 0.4,
                   delay: 0,
                   options: .curveEaseIn,
                   animations: {
                    self.createView.alpha = 1.0
    }, completion: nil
    )
  }
  
  // snap objects to grid
  func snapViewToGrid(snapView: PathView, location: CGPoint) {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2.0)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2.0)*kThickness
    var sgnX: CGFloat = 0
    var sgnY: CGFloat = 0
    if CGFloat(snapView.numberOfBlocks!).truncatingRemainder(dividingBy: 2) != 0.0 {
      sgnX = (endX - location.x).truncatingRemainder(dividingBy: kThickness)
      sgnY = (endY - location.y).truncatingRemainder(dividingBy: kThickness)
    } else {
      if snapView.orientation == "Horizontal" {
        sgnX = (endX - location.x - kThickness/2.0).truncatingRemainder(dividingBy: kThickness)
        sgnY = (endY - location.y).truncatingRemainder(dividingBy: kThickness)
      } else {
        sgnX = (endX - location.x).truncatingRemainder(dividingBy: kThickness)
        sgnY = (endY - location.y - kThickness/2.0).truncatingRemainder(dividingBy: kThickness)
      }
    }
    
    var xDistFromCenter: CGFloat = 0
    var yDistFromCenter: CGFloat = 0
    if sgnX != 0 {
      xDistFromCenter = kThickness - sgnX
    } else {
      xDistFromCenter = -sgnX
    }
    if sgnY != 0 {
      yDistFromCenter = kThickness - sgnY
    } else {
      yDistFromCenter = -sgnY
    }
    
    if CGFloat(snapView.numberOfBlocks!).truncatingRemainder(dividingBy: 2) != 0.0 {
      if snapView.orientation == "Horizontal" {
        xDistFromCenter -= kThickness/2.0
        yDistFromCenter -= kThickness/2.0
      } else {
        xDistFromCenter -= kThickness/2.0
        yDistFromCenter -= kThickness/2.0
      }
    } else {
      if snapView.orientation == "Horizontal" {
        yDistFromCenter = yDistFromCenter - kThickness/2.0
        xDistFromCenter -= kThickness/2.0
      } else {
        xDistFromCenter = xDistFromCenter - kThickness/2.0
        yDistFromCenter -= kThickness/2.0
      }
    }
    UIView.animate(withDuration: 0.1, animations: {
      snapView.center.x = location.x - xDistFromCenter + 0.5
      snapView.center.y = location.y - yDistFromCenter + 0.5
    })
  }
  
  func snapBallToGrid(snapBall: BallView, location: CGPoint) {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2.0)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2.0)*kThickness
    var sgnX: CGFloat = 0
    var sgnY: CGFloat = 0
    sgnX = (endX - location.x).truncatingRemainder(dividingBy: kThickness * 1.0)
    sgnY = (endY - location.y).truncatingRemainder(dividingBy: kThickness * 1.0)
    
    var xDistFromCenter: CGFloat = 0
    var yDistFromCenter: CGFloat = 0
    if sgnX != 0 {
      xDistFromCenter = kThickness - sgnX
    } else {
      xDistFromCenter = -sgnX
    }
    if sgnY != 0 {
      yDistFromCenter = kThickness - sgnY
    } else {
      yDistFromCenter = -sgnY
    }
    xDistFromCenter -= kThickness/2.0
    yDistFromCenter -= kThickness/2.0
    UIView.animate(withDuration: 0.1, animations: {
      snapBall.center.x = location.x - xDistFromCenter + 0.5
      snapBall.center.y = location.y - yDistFromCenter + 0.5
    })
  }
  
  func snapExitToGrid(snapExit: UIView, location: CGPoint) {
    let endX = screenWidth/2.0 + (horizontalBlocksInGrid/2.0)*kThickness
    let endY = screenHeight/2.0 + (verticalBlocksInGrid/2.0)*kThickness
    var sgnX: CGFloat = 0
    var sgnY: CGFloat = 0
    sgnX = (endX - location.x).truncatingRemainder(dividingBy: kThickness * 1.0)
    sgnY = (endY - location.y).truncatingRemainder(dividingBy: kThickness * 1.0)
    
    var xDistFromCenter: CGFloat = 0
    var yDistFromCenter: CGFloat = 0
    if sgnX != 0 {
      xDistFromCenter = kThickness - sgnX
    } else {
      xDistFromCenter = -sgnX
    }
    if sgnY != 0 {
      yDistFromCenter = kThickness - sgnY
    } else {
      yDistFromCenter = -sgnY
    }
    xDistFromCenter -= kThickness/2.0
    yDistFromCenter -= kThickness/2.0
    UIView.animate(withDuration: 0.1, animations: {
      snapExit.center.x = location.x - xDistFromCenter + 0.5
      snapExit.center.y = location.y - yDistFromCenter + 0.5
    })
  }
  
  // setting create object views
  func setupCreatePathView() {
    deleteViewBtn.isHidden = true
    modifyViewBtn.isHidden = true
    addViewBtn.isHidden = false
    singleColorView.isHidden = false
    multipleColorView.isHidden = true
    viewOrientation = "Vertical"
    horizontalBtn.layer.borderColor = UIColor.white.cgColor
    verticalBtn.layer.borderColor = UIColor.black.cgColor
    pickerView.selectRow(0, inComponent: 0, animated: true)
    numberOfBlocks = pickerView.selectedRow(inComponent: 0) + 1
    textField.text = "\(numberOfBlocks!)"
  }
  
  func setupCreateBallView() {
    deleteBallBtn.isHidden = true
    modifyBallBtn.isHidden = true
    addBallViewBtn.isHidden = false
    singleColorView.isHidden = true
    multipleColorView.isHidden = false
  }
  
  // alerts
  func alertify(message:String, in controller:UIViewController) {
    let alertController = UIAlertController(title: "",
                                            message: message,
                                            preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Okay", style: .cancel, handler: nil)
    alertController.addAction(okAction)
    controller.present(alertController, animated: true, completion: nil)
  }
}

// MARK: Extensions
extension ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 40
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerData.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String {
    return "\(pickerData[row])"
  }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return colorArray.count
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let cellSize = CGSize(width: 50, height: 50 )
    return cellSize
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if collectionView == primaryColorCollectionView {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "primaryColorCell", for: indexPath) as! PrimaryColorCollectionViewCell
      cell.boundaryView.layer.borderWidth = 2.0
      cell.boundaryView.layer.borderColor = UIColor.white.cgColor
      cell.boundaryView.layer.cornerRadius = cell.frame.size.height/2.0
      cell.coloredView.backgroundColor = colorArray[indexPath.item]
      cell.coloredView.layer.cornerRadius = cell.frame.size.height/2.0 - 5
      if indexPath == primarySelectedIndexpath {
        cell.boundaryView.layer.borderColor = colorArray[indexPath.item].cgColor
      }
      return cell
    } else if collectionView == secondaryColorCollectionView {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "secondaryColorCell", for: indexPath) as! SecondaryColorCollectionViewCell
      cell.boundaryView.layer.borderWidth = 2.0
      cell.boundaryView.layer.borderColor = UIColor.white.cgColor
      cell.boundaryView.layer.cornerRadius = cell.frame.size.height/2.0
      cell.coloredView.backgroundColor = colorArray[indexPath.item]
      cell.coloredView.layer.cornerRadius = cell.frame.size.height/2.0 - 5
      if indexPath == secondarySelectedIndexpath {
        cell.boundaryView.layer.borderColor = colorArray[indexPath.item].cgColor
      }
      return cell
    } else {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pathColorCell", for: indexPath) as! PathColorCollectionViewCell
      cell.boundaryView.layer.borderWidth = 2.0
      cell.boundaryView.layer.borderColor = UIColor.white.cgColor
      cell.boundaryView.layer.cornerRadius = cell.frame.size.height/2.0
      cell.coloredView.backgroundColor = colorArray[indexPath.item]
      cell.coloredView.layer.cornerRadius = cell.frame.size.height/2.0 - 5
      if indexPath == selectedPathColorIndexpath {
        cell.boundaryView.layer.borderColor = colorArray[indexPath.item].cgColor
      }
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if collectionView == primaryColorCollectionView {
      primarySelectedIndexpath = indexPath
      primaryColorCollectionView.reloadData()
    }
    if collectionView == secondaryColorCollectionView {
      secondarySelectedIndexpath = indexPath
      secondaryColorCollectionView.reloadData()
    }
    if collectionView == pathColorCollectionView {
      selectedPathColorIndexpath = indexPath
      pathColorCollectionView.reloadData()
    }
  }
}

extension UIColor {
  
  // MARK: Initialization
  
  convenience init?(hex: String) {
    var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
    hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
    
    var rgb: UInt32 = 0
    
    var r: CGFloat = 0.0
    var g: CGFloat = 0.0
    var b: CGFloat = 0.0
    var a: CGFloat = 1.0
    
    let length = hexSanitized.characters.count
    
    guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
    
    if length == 6 {
      r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
      g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
      b = CGFloat(rgb & 0x0000FF) / 255.0
      
    } else if length == 8 {
      r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
      g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
      b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
      a = CGFloat(rgb & 0x000000FF) / 255.0
      
    } else {
      return nil
    }
    
    self.init(red: r, green: g, blue: b, alpha: a)
  }
  
  // MARK: - Computed Properties
  
  var toHex: String? {
    return toHex()
  }
  
  // MARK: - From UIColor to String
  
  func toHex(alpha: Bool = false) -> String? {
    guard let components = cgColor.components, components.count >= 3 else {
      return nil
    }
    
    let r = Float(components[0])
    let g = Float(components[1])
    let b = Float(components[2])
    var a = Float(1.0)
    
    if components.count >= 4 {
      a = Float(components[3])
    }
    
    if alpha {
      return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
    } else {
      return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
    }
  }
  
}
